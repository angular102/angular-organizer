import { Component, OnInit } from '@angular/core';
import {DateService} from "../shared/date.services";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Tasks, TaskServices} from "../shared/task.services";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-organaizer',
  templateUrl: './organaizer.component.html',
  styleUrls: ['./organaizer.component.scss']
})
export class OrganaizerComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  tasks: Tasks[] = [];

  constructor(
    public dateService: DateService,
    public taskService: TaskServices
  ) { }

  ngOnInit(): void {
    this.dateService.date.pipe(
      switchMap(value => this.taskService.load(value))
    )
      .subscribe(
        tasks => {
          this.tasks = tasks
        })

    this.form = new FormGroup({
      title: new FormControl('', Validators.required),
    })
  }

  submit() {
    const {title} = this.form.value

    // @ts-ignore
    const task: Tasks = {
      title,
      date: this.dateService.date.value.format('DD-MM-YYYY')
    }

    this.taskService.create(task).subscribe(
      (task) => {
        this.tasks.push(task)
          this.form.reset()
        },
      (error) => {
        console.error(error)
      }
    )
  }

  remove(task: Tasks) {
    this.taskService.remove(task).subscribe(
      () => {
        this.tasks = this.tasks.filter(t => t.id !== task.id)
      },
    (err) => console.error(err)
    )
  }

}
